import {Component, Input} from '@angular/core';
import {Order} from "./order.model";

@Component({
  selector: 'app-fast-food',
  templateUrl: './fast-food.component.html',
  styleUrls: ['./fast-food.component.css']
})
export class FastFoodComponent  {
  order = [new Order()];

  items = [
    {name: 'Hamburger', price: 80, icon: 'https://media.gettyimages.com/photos/burger-picture-id182744943?s=612x612'},
    {name: 'Coffee', price: 70, icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Roasted_coffee_beans.jpg/1200px-Roasted_coffee_beans.jpg'},
    {name: 'Cheeseburger', price: 90, icon: 'https://images.immediate.co.uk/production/volatile/sites/2/2020/04/Cheesburger-01e0a43.jpg?quality=90&resize=768,574'},
    {name: 'Tea', price: 50, icon: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delish-210419-iced-tea-02-landscape-jg-1619020612.jpg?crop=1.00xw:1.00xh;0,0&resize=640:*'},
    {name: 'Fries', price: 45, icon: 'https://www.seriouseats.com/thmb/_BkW9V2wK3Zed-zQAETkRSJS8ac=/1500x1125/filters:fill(auto,1)/__opt__aboutcom__coeus__resources__content_migration__serious_eats__seriouseats.com__2018__04__20180309-french-fries-vicky-wasik-15-5a9844742c2446c7a7be9fbd41b6e27d.jpg'},
    {name: 'Cola', price: 40, icon: 'https://images.unsplash.com/photo-1561758033-48d52648ae8b?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8Y29jYSUyMGNvbGF8ZW58MHx8MHx8&ixlib=rb-1.2.1&w=1000&q=80'},
  ]

  orderDetails = 'Order is empty! Please, add some items!'

  onAddItem(index: number) {
    this.order.push({
      name: this.items[index].name,
      price: this.items[index].price
    });
    this.orderDetails = ``;
  }

  deleteItem(i: number) {
    this.order.splice(i, 1);
  }



}
