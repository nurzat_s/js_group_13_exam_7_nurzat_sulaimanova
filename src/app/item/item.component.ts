import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent  {
  @Input() name = 'Hamburger';
  @Input() price = 80;
  @Input() icon = 'https://media.istockphoto.com/photos/cheeseburger-isolated-on-white-picture-id1157515115?k=20&m=1157515115&s=612x612&w=0&h=1-tuF1ovimw3DuivpApekSjJXN5-vc97-qBY5EBOUts='
  @Output() delete = new EventEmitter();
  getFullItemDetail() {
    return `${this.name} price: ${this.price}`;
  }

}
